import Vue from 'vue'
import Vuex from 'vuex'

import FieldService from '@/FieldService';
const fieldService = new FieldService();

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        influencers: []
    },
    mutations: {        
        set (state, [variable, value]) {
          state[variable] = value
        },
    },
    actions: {
        async getInfluensers({commit}) {
          commit('set', ['influencers', await fieldService.getInfluencers()])
        },
    }
})